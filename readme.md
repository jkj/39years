# 39 Years

## Doc

* [md](doc/39years.md)
* [pdf](doc/39years.pdf)

## Demo

[play in browser](https://stvle.s3.amazonaws.com/39years/index.html)



## Locations

Manor Ground Floor:
![](doc/39map-ground1-a.png)

Manor first floor:
![](doc/39map-upper5-a.png)

## Building

You need `emscripten` in your path. Check with, `emcc -v`, which will print version

Otherwise see [emscripten.org](https://emscripten.org/) to install.

Here is a quick start example for Windows:

```
see;
https://emscripten.org/docs/getting_started/downloads.html

d:\
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
emsdk install latest
emsdk activate latest
emsdk_env.bat

Add to PATH:
d:\emsdk\upstream\emscripten;d:\emsdk\upstream\emscripten\tools

* verify emscripten
emcc -v
```

With `emcc` and `em++` in your path;

```
cd strands/games/39years
make
```

This will build the `web` directory.

## Deploying

After building the `web` directory, you can copy the content of this directory to your server, rename it to say `39years`, then point a browser at it.

## Running Locally

You need a local web server. For example, you can run using python like this:

```
start http://localhost:8000/index.html
python -m http.server -d web
```

You can also run with PHP if you have that installed.

```
start http://localhost:8000/index.html
php -S 127.0.0.1:8000 -t web
```


























