#pragma once

#include <list>
#include <string>
#include <assert.h>
#include "utils.h"
#include "logged.h"

#include "imgui_markdown.h"
#include "IconsFontAwesome5.h" 

#define TAG_IMTEXT  "ImText, "

struct ImText
{
    typedef std::string string;
    struct Par
    {
        enum partype
        {
            par_void = 0,
            par_text,
            par_img,
        };

        Par(ImText* host, partype t) : _host(host), _type(t) {}
        virtual ~Par() {}

        ImText*         _host;
        partype         _type;


        virtual int size() const { return 0; }
    };

    struct ParText: public Par
    {
        string          _text;
        bool            _hilite = false;

        ParText(ImText* host, const char* s, bool hilite)
            : Par(host, par_text), _text(s), _hilite(hilite) {}
        
        int size() const override { return _text.size(); }
    };

    struct ParImg: public Par
    {
        ImTex   _img;

        ParImg(ImText* host, const ImTex& ii) : Par(host, par_img), _img(ii) {}
        ~ParImg()
        {
            if (_img.destroy()) --_host->_imgCount;
        }
        
    };


    typedef std::list<Par*> Pars;

    Pars        _pars;
    bool        _changed = false;
    int         _maxText = 1024*4;
    int         _size = 0;
    int         _maxImg = 3;
    int         _imgCount = 0;

    // set when we click on a command link
    // this is picked up by yieldCmd
    string      _clickCommand;

    // markdown
    ImGui::MarkdownConfig       _mdConfig;
    ImFont* H1 = NULL;  // bold big
    ImFont* H2 = NULL;  // italic
    ImFont* H3 = NULL;  // bold

    void setBoldFont(ImFont* f)
    {
        H1 = H3 = f;
    }

    void setItalicFont(ImFont* f)
    {
        H2 = f;
    }

    ImText()
    {
        _mdConfig.linkCallback = &_linkCallback;
        _mdConfig.tooltipCallback = NULL;
        _mdConfig.imageCallback = &_imageCallback;
        _mdConfig.linkIcon = ICON_FA_LINK;
        _mdConfig.formatCallback = &_formatCallback;
        _mdConfig.userData = (void*)this;
    }

    ~ImText()
    {
        clear();
    }

    void clear()
    {
        purge(_pars);
        _size = 0;
        _imgCount = 0;
    }

    void add(const char* s, bool hitlite = false)
    {
        if (s && *s)
        {
            //LOG1(TAG_IMTEXT "adding '", s << "'");
            // whole string will be a single par regardless

            Par* p = new ParText(this, s, hitlite);
            _add(p);
        }
    }

    void add(const ImTex& ii)
    {
        Par* p = new ParImg(this, ii);
        _add(p);
    }

    void linkCallback(ImGui::MarkdownLinkCallbackData d)
    {
        // called when a link is clicked
        if (!d.isImage)
        {
            string url(d.link, d.linkLength);

            if (startsWith(url, "http://") || startsWith(url, "https://"))
            {
                LOG1(TAG_IMTEXT "link clicked ", url);
                
#ifdef __EMSCRIPTEN__
	/* Implementation in pre.js */
            EM_ASM({ if(window["open_url"]) window.open_url($0, $1) }, url.c_str(), url.length());
#endif
            }
            else if (!url.empty())
            {
                // treat as a command, send back to game

                LOG1(TAG_IMTEXT "command clicked ", url);
                _clickCommand = url;
            }
        }
    }

    ImGui::MarkdownImageData imageCallback(ImGui::MarkdownLinkCallbackData d)
    {
        string url(d.link, d.linkLength);
        //LOG1(TAG_IMTEXT "imagecallback ", url);

        assert(d.isImage);
        
        ImGui::MarkdownImageData imageData;

        ImTex* tex = texLoader.get(url);
        if (tex)
        {
            assert(tex->_tid && tex->_w > 0 && tex->_h > 0);
            
            imageData.isValid =         true;
            //imageData.useLinkCallback = false;
            imageData.user_texture_id = tex->_tid;
            imageData.size = ImVec2(tex->_w, tex->_h);

            // For image resize when available size.x > image width, add
            ImVec2 const contentSize = ImGui::GetContentRegionAvail();
            if( imageData.size.x > contentSize.x )
            {
                float const ratio = imageData.size.y/imageData.size.x;
                imageData.size.x = contentSize.x;
                imageData.size.y = contentSize.x*ratio;
            }
        }
        return imageData;
    }

    void formatCallback(const ImGui::MarkdownFormatInfo& info, bool start)
    {
        bool done = false;
     
        switch(info.type )
        {
        case ImGui::MarkdownFormatType::EMPHASIS:
            {
                const ImGui::MarkdownHeadingFormat* fmt = 0;
                done = true;
                
                if (info.level == 1 )
                {
                    // normal emphasis (italic)
                    fmt = &info.config->headingFormats[ImGui::MarkdownConfig::NUMHEADINGS - 2 ];
                }
                else
                {
                    // strong emphasis
                    fmt = &info.config->headingFormats[ImGui::MarkdownConfig::NUMHEADINGS - 1 ];
                }

                if (fmt && fmt->font)
                {
                    if (start) ImGui::PushFont(fmt->font);
                    else ImGui::PopFont();
                }
                else
                {
                    if (start)
                        ImGui::PushStyleColor( ImGuiCol_Text, ImGui::GetStyle().Colors[ ImGuiCol_TextDisabled ] );
                    else ImGui::PopStyleColor();
                }
            }
            break;
        }

        if (!done)
            ImGui::defaultMarkdownFormatCallback(info, start);
    }

    void renderMarkdown(const string& t)
    {
        //ImGui::TextWrapped("%s", t.c_str());
        
        _mdConfig.headingFormats[0] =    { H1, true };
        _mdConfig.headingFormats[1] =    { H2, true };
        _mdConfig.headingFormats[2] =    { H3, false };
        
        ImGui::Markdown(t.c_str(), t.length(), _mdConfig);
    }

    void render()
    {
        Pars::iterator it = _pars.begin();
        Pars::iterator ie = _pars.end();
        
        while (it != ie)
        {
            const Par* p = *it;
            ++it;

            bool last = (it == ie);

            switch (p->_type)
            {
            case Par::par_text:
                {
                    ParText* pt = (ParText*)p;
                    ImVec4 col;

                    bool lastText = last;

                    if (!lastText)
                    {
                        // are we actually the last text though?
                        lastText = true;
                        Pars::iterator i = it;
                        while (i != ie)
                        {
                            if ((*i)->_type == Par::par_text)
                            {
                                // no, more text follows
                                lastText = false;
                                break;
                            }
                            ++i;
                        }
                    }
                    
                    if (lastText)
                    {
                        if (pt->_hilite)
                        {
                            col = ImGui::GetStyleColorVec4(ImGuiCol_HeaderActive);
                        }
                        else col = ImGui::GetStyleColorVec4(ImGuiCol_Text);
                    }
                    else
                    {
                        if (pt->_hilite)
                        {
                            col = ImGui::GetStyleColorVec4(ImGuiCol_Header);
                        }
                        else
                        {
                            col = ImGui::GetStyleColorVec4(ImGuiCol_TextDisabled);
                        }
                    }
            
                    ImGui::PushStyleColor(ImGuiCol_Text, col);
                    renderMarkdown(pt->_text);
                    ImGui::PopStyleColor();
                }
                break;
            case Par::par_img:
                {
                    ParImg* pi = (ParImg*)p;
                    int w = pi->_img._w;
                    int h = pi->_img._h;

                    ImVec2 uv_min = ImVec2(0.0f, 0.0f); // Top-left
                    ImVec2 uv_max = ImVec2(1.0f, 1.0f); // Lower-right

                    // No tint
                    ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
                    
                    // 50% opaque white
                    ImVec4 border_col = ImVec4(1.0f, 1.0f, 1.0f, 0.5f);

                    if (last)
                    {
                        border_col = ImGui::GetStyleColorVec4(ImGuiCol_Text);
                    }
                    else
                    {
                        border_col = ImGui::GetStyleColorVec4(ImGuiCol_TextDisabled);
                    }

                    //LOG1("rendering image ", pi->_img._name << " w:" << w << " h:" << h);
                    
                    int vw = ImGui::GetContentRegionAvail().x;
                    int padw = vw - (w + 2); // border
                    padw /= 2;
                    
                    if (padw < 0)
                    {
                        double sc = ((double)vw)/(w + 2);
                        h = sc * h;
                        w = vw - 2;
                    }

                    if (padw > 0)
                    {
                       ImGui::Indent(padw);
                    }
                    
                    ImGui::Image(pi->_img._tid, ImVec2(w, h), uv_min, uv_max, tint_col, border_col);
                    if (padw > 0)
                    {
                        ImGui::Unindent(padw);
                    }
                    
                }
                break;
            }
        }
    }

protected:
    
    static void _formatCallback(const ImGui::MarkdownFormatInfo& info,
                                bool start)
    {
        assert(info.config);
        ImText* h = (ImText*)info.config->userData;
        assert(h);
        h->formatCallback(info, start);
    }

    static ImGui::MarkdownImageData _imageCallback(ImGui::MarkdownLinkCallbackData d)
    {
        ImText* h = (ImText*)d.userData;
        assert(h);
        return h->imageCallback(d);
    }
    
    static void _linkCallback(ImGui::MarkdownLinkCallbackData d)
    {
        ImText* h = (ImText*)d.userData;
        assert(h);
        h->linkCallback(d);
    }

    void _add(Par* p)  
    {
        // consume p
        _pars.push_back(p);
        _size += p->size();
        if (p->_type == Par::par_img) ++_imgCount;
        _trim();
        _changed = true;
    }

    void _trim()
    {
        if (_maxText)
        {
            while (_size > _maxText)
            {
                // this can also remove images, when they are
                // part of "old text".
                Pars::iterator it = _pars.begin();
                assert(it != _pars.end());
                Par* p = *it;
                _pars.pop_front();
                _size -= p->size();
                assert(_size >= 0);
                //LOG1("trimming text ", (const char*)*p);
                delete p;
            }
        }
        if (_maxImg && _imgCount > _maxImg)
        {
            auto it = _pars.begin();
            while (it != _pars.end())
            {
                if ((*it)->_type == Par::par_img)

                {
                    ParImg* pi = (ParImg*)(*it);
                    //LOG1("trimming image ", pi->_img._name);
                    it = _pars.erase(it);
                    delete pi; // adjusts imgCount
                    if (_imgCount <= _maxImg) break; // now ok.
                }
                else ++it;
            }
        }
    }
    
    
    
};
