#pragma once

#include <string>
#include <assert.h>
#include <functional>
#include <list>
#include "stb_image.h"

#define TAG_IMTEX "ImTex, "

struct ImTex
{
    typedef std::string string;
    
    string  _name;
    ImTextureID  _tid = 0;
    int _w;
    int _h;
    int _chans;

    bool operator<(const ImTex& t) const { return _name < t._name; }

    bool create(const char* name, const unsigned char* data, int sz)
    {
        assert(!_tid);
        
        _name = name;
        
        const int desired_channels = 4;
        stbi_uc* pixels = stbi_load_from_memory((const unsigned char*)data,
                                                sz,
                                                &_w, &_h,
                                                &_chans,
                                                desired_channels);

        if (pixels)
        {
            //LOG1("Loaded image got pixels ", name);
            sg_image_desc sgid = {};
            sgid.width = _w;
            sgid.height = _h;
            sgid.pixel_format = SG_PIXELFORMAT_RGBA8;
            sgid.min_filter = SG_FILTER_LINEAR;
            sgid.mag_filter = SG_FILTER_LINEAR;
            sgid.wrap_u = SG_WRAP_CLAMP_TO_EDGE;
            sgid.wrap_v = SG_WRAP_CLAMP_TO_EDGE;
            auto& si = sgid.data.subimage[0][0];
            si.ptr = pixels;
            si.size = (size_t)(_w * _h * 4);  // XX

            sg_image sgi = sg_make_image(&sgid);
            stbi_image_free(pixels);
            _tid = (ImTextureID)sgi.id;
        }
        else
        {
            LOG1(TAG_IMTEX "Image cannot decode", _name);
        }
        return _tid != 0;
    }

    bool destroy()
    {
        if (_tid)
        {
            sg_image sgi;
            sgi.id = (uint32_t)_tid;
            sg_destroy_image(sgi);
            _tid = 0;
            return true;
        }
        return false;
    }

    bool valid() const { return _tid != 0; }
};

struct ImTexLoader
{
    typedef std::string string;
    typedef std::function<void(ImTex)> Loaded;
    typedef std::list<ImTex> Pool;

    Fetcher& _fetcher;
    Pool     _pool;

    struct Req
    {
        string          _name;
        Loaded          _loaded;
        bool            _pending = false;
        bool            _failed = false;
    };

    typedef std::list<Req>    Requests;
    Requests            _r;
    
    ImTexLoader(Fetcher& f) : _fetcher(f) {}

    ~ImTexLoader()
    {
        _purge(); 
    }

    void loaded(const char* name)
    {
        Requests::iterator it;
        bool found = false;
        for (it = _r.begin(); it != _r.end(); ++it)
        {
            if (it->_name == name)
            {
                found = true;
                break;
            }
        }

        if (found)
        {
            ImTex tex;
            Req r = *it;
            
            if (fetcher.ok)
            {
                int sz;
                char* fdata = fetcher.yield(sz);

                LOG1("Loaded image ", name << " data size:" << sz);
                tex.create(name, (const unsigned char*)fdata, sz);
                delete fdata;

                _r.erase(it);
                if (r._loaded) (r._loaded)(tex);
                else
                {
                    // if no callback, add to pool
                    _pool.push_back(tex);
                }
            }
            else
            {
                LOG1(TAG_IMTEX "failed to load ", name);

                // leave in queue, but mark failed so we dont
                // try loading it again
                it->_failed = true;
            }
        }
        else
        {
            LOG1(TAG_IMTEX "not found", name);
        }
    }


    void load(const char* path, Loaded loaded)
    {
        for (auto& i : _r)
            if (i._name == path) return; // already queued
        
        Req r;
        r._name = path;
        r._loaded = loaded;
        _r.push_back(r);
    }

    void load(const string& path, Loaded loaded) { load(path.c_str(), loaded); }

    ImTex* get(const string& path)
    {
        /* if the texture is already loaded in the pool, return it
         * otherwise perform a fetch and wait to be called later
         */

        for (auto& i : _pool)
        {
            if (i._name == path) return &i;
        }

        // not here, load it
        load(path, Loaded());
        return 0;
    }

    void poll()
    {
        for (auto& r : _r)
        {
            if (r._pending || r._failed) continue;
            
            if (fetcher.start(r._name, _loaded, this))
            {
                r._pending = true;
                LOG1("requesting image ", r._name);
            }

            break;
        }
    }

private:

    static void _loaded(const char* name, void* ctx)
    {
        assert(ctx);
        ImTexLoader* tl = (ImTexLoader*)ctx;
        tl->loaded(name);
    }

    void _purge()
    {
        for (auto& i : _pool) i.destroy();
        _pool.clear();
    }

};


