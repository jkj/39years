# 39 Years Design Overview

## Game Mechanic

> Some games force the player to connect up information and make deductions – they can be merciless about consequences if you guess wrong. Other games are more about the clue-hunting; once you find the correct clues, the game leads you to the correct solution.

This will be a "clue hunting" game.

> The story outline here is single-threaded. Can there be different outcomes from each chapter? Does that affect the next chapter? If you screw up, can a character die early? What then? Lots and lots to decide.

Yes, single threaded story with minor alternative scenes or scene alterations, but no main branches.

Chapter outcomes: The same, possibly with minor variations.

Affect subsequent chapters: Only in terms of text variations.

Screw up: You can't. A chapter will end when you've completed it correctly.

Die early: Can't happen.

Zarf's examples:

> The Frogwares Sherlock Holmes games (particularly recent ones like “The Devil’s Daughter”) have a mind-map interface where you must combine pairs of clues to draw deductions. Then you combine the deductions with more clues. Eventually you can reach a conclusion which allows you to complete the case. There is a right answer but the game allows you to select a wrong one if you combine clues that don’t work together.

You collect clues and "you" or your sidekick makes deductions. Clues are conceptually combined automatically as you find them.

> Return of the Obra Dinn goes deep into physical evidence. You must examine scenes and figure out who is who. However, the game is entirely about identifying bodies. There’s no question of motive, testimony, or lies.

Challenges will be based on information, eluded from conversation with people.
For example, you will learn of a fact that, at the time, appears of no relevance, then later perhaps find an object that puts that fact into importance.


> A Case of Distrust has you collecting statements from people and then confronting them with other statements. Or combinations of statements. If you catch someone in a contradiction, that’s progress.

Yes, this sort of thing will be used. However, as above you may have learnt the importance of a "fact", then you can use this fact by confronting or accusing people, which will lead to information divulged.

> Paradise Killer is entirely about running around collecting clue objects. Once you have all the clues, the case pretty much solves itself. Fun ride though.

You won't collect objects per se but facts. The case solves itself when you have enough facts, _but_ to get sufficient facts you will need to _use_ what you know so far on people. 

> Disco Elysium is set up as a cop procedural. It plays out as an RPG where your skills are detective skills rather than combat skills. (Observing clues, interrogating people, making intuitive leaps, drinking heavily, bullshitting, smashing down doors, getting beat up…) (You know, the things detectives do in detective stories.)

There won't be any combat. I know some of the Sherlock Holmes movies had him doing "king foo". Maybe the sequel :)

> The Flower Collectors has you sitting on a balcony watching a city square with binoculars. Watch everybody, spot what they do.

Interesting idea, perhaps worth using for one clue. For example, you have to "hide" and observe or listen to learn something. A bit like Sherlock Holmes disguises.

> Lucifer Within Us and Unheard both feature a timeline that you can scrub back and forth, tracking suspects as they take actions in various locations.

There will be a scrubbable timeline! This is an engine feature generally for all games. However, this won't be used directly for the game mechanic. It's main purpose will be to "undo" to previous checkpoints.

Despite the plot being almost linear, there will nevertheless be short branch dead-ends, where the game stops, either because you die (murdered by the murderer?) or the game allows you to something that means you cannot continue.

These dead-ends can be a great source of comedy, which even serious games need a small amount.

You will be able to scrub the timeline back to wherever you like.

---------------------

Thanks for these, all most interesting. Here's my feeling of how the detective mechanic should work:

You collect facts/clues and "you" or your sidekick makes deductions. Clues are conceptually combined automatically as you find them. Your side-kick (detective assistant in this game), will essentially be the online-help.

Challenges will be based on collecting information, mostly eluded from conversation with people. For example, you will learn of a fact that, at the time, appears of no relevance, then later find an object that makes that fact important.

> A Case of Distrust has you collecting statements from people and then confronting them with other statements. Or combinations of statements. If you catch someone in a contradiction, that’s progress.

Yes, this sort of thing will be used. You can use facts discovered to confront or accuse people, which will lead to additional information divulged.

You won't collect objects per se but facts. In fact there will be very few objects you can pick up and manipulate. The case solves itself when you have enough facts, _but_ to get sufficient facts you will need to _use_ what you know already on people. eg `show X to Y` or `tell X about Y`. Although this won't necessarily be done via parser input.

There won't be any combat. I know some of the Sherlock Holmes movies had him doing "kung foo". Maybe the sequel :)

> The Flower Collectors has you sitting on a balcony watching a city square with binoculars. Watch everybody, spot what they do.

Interesting idea, perhaps worth using for one clue. For example, you have to "hide" and observe or listen to learn something. A bit like Sherlock Holmes disguises.

> Lucifer Within Us and Unheard both feature a timeline that you can scrub back and forth

Yes! There will be a scrubbable timeline. This is an general engine feature for all games. However, this won't be used directly for the game mechanic. Its main purpose will be to "undo" to previous checkpoints.

Despite the plot being almost linear, there will nevertheless be short branch dead-ends, where the game stops, either because you die (murdered by the murderer?) or the game allows you to something that means you cannot continue.

These dead-ends can be a great source of levity, which even serious games need a small amount.

You will be able to scrub the timeline back to wherever you like.

Some points about parser input vs choices:

I see this as a parser-choice hybrid. You should be able to complete the game just from choices, although they will be highly context sensitive. You will also be able to do parser input.

Character interaction will be predominantly choice-based conversation, although you _could_ also type `> ask X about Y`. etc. This will mostly provide more background information.

Any wild tangents will be parser based, eg `> punch the sergeant`.




























