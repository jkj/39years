# The 39 Years

## Story Summary

After a long and successful career as a Private Detective spanning the 20s, 30s and 40s, Marvin (Marv) West is finally retired. The golf and fishing isn't quite as thrilling as it sounds and the opportunity to take on one last case is too much to resist. It doesn't have to be anything heavy, he figures, he's been contacted about an inheritance dispute and it seems just the tonic. But when the bodies start turning up, Marv starts to realise that there's more to this than he originally thought.

We have the one Player Character nine Non-Player Character's all of varying but linked relationships to each other. Four of them are murdered, one escapes attempted murder at the end thanks to Marv but it's a shallow victory. The murders are carried out by different NPCs for their own reasons, and eventually they themselves die, so we're interchanging murderers and victims.

It's implied (but never stated for legal reasons) that this is a "spiritual" successor to Deadline. The title "The 39 Years" refers to how many years ago Deadline was released, hence Marv's age of 69 as well as I've assumed that the Detective in Deadline was 30.

* _Presumably the Player is Marv, that only leaves 7 main NPCs, since Elliot is dead._

## Characters

* Marvin West, 69. Retired Private Detective. Recently retired but a little bored, Marvin (Marv) can't resist taking on one last case.

* Genevieve Williams, 29. Current Private Detective, mentored by Marv, mentions the inheritance dispute to Marv. Has more than a passing interest in the case.

* Elliot Musk (dead), 82. Background Character. Elliot is a business mogul with a large estate that is intensely sought after.

* Annabelle Trevelyan (formerly Musk, formerly Long), 49. Supporting Character. Annabelle is Elliot's cunning daughter.

* Gregory Musk, 52. Background Character. Gregory is Elliot's son. Anger can often get the best of him.

* Rupert Trevelyan, 55. Background Character. Rupert is Annabelle's husband. He secretly has an affair with an exotic dancer.

* Elizabeth Adams, 34. Background Character. Elliot's nurse and Gregory's secret lover.

* Robert Houseman, 77. Background Character. Elliot's old business partner. He sent Elliot to jail for his crimes.

* Alison Frasier, 44. Background Character. Alison is the VP of Elliot's company, open to bribery & corruption.

_Other incidental characters:_

* Police sergeant.

* Police constable.

## The Murders

### The First Murder

* Victim: Gregory Musk

* Method of Death: Allergy Poisoning

* Murderer: Elizabeth Adams

* Motive: Elizabeth only had an affair with Gregory in order to get the family money. However, knowing that she was on the will, she realises there was no need to keep him around.

### The Second Murder

* Victim: Rupert Trevelyan

* Method of Death: False suicide

* Murderer: Genevieve Williams

* Motive: Annabelle's arrest was the perfect opportunity to off Rupert and reduce the number of remaining heirs.

### Third Murder

* Victim: Elizabeth Adams

* Method of Death: Beating

* Murderer: Robert Houseman

* Motive: When Robert's secret is revealed, he fears that Elizabeth will use it against him. He acts in a blind panic.

### The Fourth Murder/Death (accidental)

* Victim: Robert Houseman

* Method of Death: Heart Attack

* Murderer: Robert Houseman

* Robert gives himself a heart attack.

### The Fifth Murder (Attempted)

* Victim: Genevieve Williams

* Murderer: Alison Frasier

* Motive: Alison connected the dots, and realised that if she didn't kill Genevieve she would likely end up dead herself.

## Game Sequence Outline

### Sequence One

A prospective client, Annabelle Trevelyan, asks Marvin to come out of retirement to help her with an inheritance dispute. According to Annabelle, her father, Elliot, died very suddenly and recently, and Annabelle believes his death relates with her brother, Gregory's, recent grab for the family fortune. However, if Marvin can somehow prove there was foul play in the murder, then the slayer rule would apply, leaving Gregory penniless.

### Sequence Two

Marvin and Genevieve accompany Annabelle to her father's grandiose mansion, and establish lodging at a hotel nearby. Marvin and Genevieve attend Elliot's hearing. While there, Marvin takes stock of the many people who were apparently so close to Elliot that they assumed he'd write them in his will to inherit a cut of his vast estate. After talking around, Marvin concludes that Elliot was not actually well-loved, but rather, was incredibly gullible to the point where even mere acquaintances felt that they could manipulate him into giving them the entirety of his wealth.

At the hearing, Marvin realises that there is someone missing - Gregory. The hearing goes through, and as Annabelle predicted, much of Elliot's estate was planned to go to Gregory. Pieces of Elliot's estate were also given to 6 other guests (including Genevieve).

After the hearing is over, the party is released into the house's dining room for a dinner. There is a scream as they enter the dining room: Gregory's body, dead, lays hunched over on the dining table, next to a single glass of lemon water. In Gregory's hand is a scrap of paper. Police are called.

* _Can allergic reaction kill that quickly?_

### Sequence Three

Marvin examines the scrap of paper. The scrap seems to have been written on in some kind of code. Annabelle points out that both her father and brother were deeply into murder mystery novels, and that this could be Gregory's dying message.

Marvin cracks the code after Genevieve realises that half of the code was written with lemon juice and is therefore unseeable without a UV light. With Gregory's dying message, Marvin accuses Annabelle of the murder. Police apprehend Annabelle. Marvin, Genevieve, and everyone else are cleared to leave the manor. Marvin, along with the guests who had been in Elliot's will, choose to stay in the manor overnight. The next morning, Rupert Trevelyan is found hanged in his room.

### Sequence Four

Marvin and Genevieve head to Rupert's room, and finds a tape recorder.  He clicks the "play" button, and is greeted to a recording created by Rupert, claiming that he is killing himself because he cannot go on knowing that his wife is a murderer. Realising that the tape was pre-wound (meaning that Rupert was murdered and did not commit suicide), Marvin demands a full investigation of the scene, and an alibi from everyone in the manor. The police tell Marvin that the room was locked the entire time, and that nobody could have gotten in from the outside.

* _Do suicides never rewind the tape themselves?_
* _How/why did he make the recording if not suicide?_
* _Doesn't Annabelle have a key?_

Marvin talks to several of the manor guests. He learns from an investigator that Elliot had installed security cameras in many of his hallways. attempts to get a security footage tape, but realises that somebody had caused a disturbance in the security footage by using a cable connector. Marvin realises that the murderer must have known enough about the house's security system in order to mess with the security footage. He wonders if somehow Annabelle has escaped and murdered Rupert but quickly rules that idea out.

* _For what reason did he have cameras in his own hallways? Would they even be running during the day?_

Genevieve and Marvin manage to trick Elizabeth into revealing that she knows more about the security system than anyone else. Genevieve accuses Elizabeth, but Marvin isn't so sure. Elizabeth eventually breaks down, revealing that while she did mess with the cable connectors in the security system, it was long before Elliot had even died. Genevieve continues to accuse Elizabeth, saying that since Rupert's room was locked, Elizabeth was the only person who would've had access to it.  Elizabeth insists that Rupert was very private and did not like her cleaning his room, and so she did not have a key to it.

### Sequence Five

Marvin and Genevieve continue to investigate. After investigating in Elliot's study, they find an old business binder full of old papers. One of them insinuates that Elliot went to jail. They continue digging, and realise that Robert Houseman, Elliot's old business partner, had something to do with Elliot's imprisonment. They ask Robert about it.  Marvin assumes that everybody who has gotten murdered so far was written into Elliot's will. He orders everybody to go around with a "buddy" at all times. Elizabeth and Robert are paired up, and Alison is asked to follow Genevieve and Marvin.

### Sequence Six

Marvin hears a scream that sounds like Elizabeth, and he and Genevieve rush to the garden. There, he finds Elizabeth dead. He instantly starts to search for Robert, but finds that Robert has been killed as well, laying next to a statue that looks uncannily like a person.

* _statue of whom?_

Marvin goes to re-investigate a lot of the previous locations. Alison says she is scared, and decides to stick with Genevieve in the foyer of the house. Marvin notices some ash by the fireplace in Elliot's study.  He also receives the toxicology report from Gregory's death; allergic reaction from a very specific type of tree nut. Along with that, he notices that there is a second entrance into Rupert's room, a door that connects to a small passage of hallways that the servants can use to get around the house without being seen. In these passageways, he also notices a fallen journal from Elizabeth talking about her affair with Gregory. He also notices that there are dancerren's drawings on the walls, most strikingly is one of three stick figure people; a mother, daughter, and husband. The mother and daughter stick figures have the same hair colors as Annabelle and Genevieve respectively.

* _servants!! Presumably there are none there anymore._
* _dancerren??_

### Sequence Seven

Marvin, who thinks he's pieced everything together, rushes back to the foyer where he finds Alison attempting to sneakily attack Genevieve.  Marvin saves Genevieve. Alison is arrested, leaving Marvin and Genevieve to solve the rest of the mystery. Genevieve brings up motives, and she and Marvin try to devise motives for each of the potential killers in each murder.

Marvin and Genevieve learn through more investigation of Elliot's room that Alison was planning to outsource the company's workers, and lay many people off.

Marvin also notices a picture in Elliot's pocket. It's Annabelle, with a man that is not Rupert, and a daughter who looks hauntingly familiar; it's Genevieve. Connected with his knowledge of the stick figures, Marvin realises that Genevieve is Annabelle's daughter.

### Sequence Eight

The reveal: Marvin declares that he knows exactly what happened. He goes to the police and has them arrest Genevieve. Marvin explains that there wasn't just one murder, but several. And that the only non-murderer was Annabelle. But Annabelle isn't all to blame, either, because she's the one who orchestrated the entire thing by convincing her father to choose the five people for his will. Marvin also suggests that Annabelle asked Elliot to write up a second will, with only her as his successor.

With the murder solved, Marvin feels somewhat conflicted over his victory, but is nonetheless satisfied with his conclusions.

* _What is the game mechanic of the reveal? Do you accuse someone. What are the different endings?_


